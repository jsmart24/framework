<?php

class Main
{
	public function index(): string
	{
		// Вызывается по умолчанию

		// admin/main

		return '';
	}

	public function edit(): string
	{
		// GET: admin/main/edit

		return '';
	}

	public function edit_POST(): string
	{
		// POST: admin/main/edit

		return '';
	}

	public function edit_PUT(): string
	{
		// PUT: admin/main/edit

		return '';
	}

	public function edit_DELETE(): string
	{
		// DELETE: admin/main/edit

		return '';
	}

	// Если не прописаны роуты - работает по такому алгоритму
}