<?php

namespace JSmart\Core\Session\Handlers;

use JSmart\Core\Session\SessionInterface;

class BaseHandler implements SessionInterface
{
	public function put($key, $value, $seconds){}
	public function get($key, $default_value){}
}