<?php

namespace JSmart\Core\Session;

interface SessionInterface
{
	public function put(string $key, $value, int $seconds);

	public function get(string $key, $default_value);
}